import { createStore } from 'vuex'
import axios from 'axios'

export default createStore({
  state: {
    allData: {},
    loader: true,
    filter: {
      query: '',
    }
  },

  mutations: {
    SET_DATA(state, gettedData){
      state.allData = gettedData
    },

    SET_QUERY(state, query){
      state.filter.query = query
    },

    SET_LOADER(state, value) {
      state.loader = value
    }
  },

  actions: {
    async getData({commit}){
      var data = await axios.get('https://apitesting.plerk.io/v2/category', {
        headers: {
                Authorization: 'Bearer ' + '5bc95bf034d900548243a59e2296bd683729bd75057879fd0f877d3adc7d1db6bedbfccb47aca04e44ef28adf4c3e9e72afe2f2b295b3bf08e2a47ec75f9607d'
        }
      })
      const gettedData = data.data.data
      commit('SET_DATA', gettedData)
      var loaderValue = false
      commit('SET_LOADER', loaderValue)
    }
  },

  getters: {
    filteredData(state){
      let products = state.allData
      if(state.filter.query.length >= 1) {
        return products.filter(p => p.name.esp.toLowerCase().includes(state.filter.query.toLowerCase()))
      }
      return state.allData
    }
  }
})
